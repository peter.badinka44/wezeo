import { createRouter, createWebHistory } from '@ionic/vue-router';

const routes = [
  {
    path: '/',
    component: () => import('../pages/home/Home.vue')
  },
  {
    path: '/themes/:slug',
    component: () => import('../pages/themes/Theme.vue')
  },
  {
    path: '/courses/:slug',
    component: () => import('../pages/courses/Courses.vue')
  },
  {
    path: '/legacy-course/:slugCourse/:slugLesson',
    component: () => import('../pages/course/Course.vue')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router

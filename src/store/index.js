import {createStore} from 'vuex'

import app from './app-store'

const store = createStore({
  modules: {
    app,
  }
})

export default store
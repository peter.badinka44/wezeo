import { createApp } from 'vue'

import App from './App.vue'
import BaseLayout from './components/BaseLayout.vue'
import router from './router'
import store from './store'

import { IonicVue } from '@ionic/vue'

/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css'

/* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css'
import '@ionic/vue/css/structure.css'
import '@ionic/vue/css/typography.css'

/* Optional CSS utils that can be commented out */
import '@ionic/vue/css/padding.css'
import '@ionic/vue/css/float-elements.css'
import '@ionic/vue/css/text-alignment.css'
import '@ionic/vue/css/text-transformation.css'
import '@ionic/vue/css/flex-utils.css'
import '@ionic/vue/css/display.css'

/* Theme variables */
import './theme/variables.css'
import './theme/core.css'

/* Ionic components */
import {
	IonImg,	
	IonThumbnail,
	IonRow,
	IonCol,
	IonCard,
	IonCardHeader,
	IonCardTitle,
	IonChip,
	IonText,
  IonItem,
} from '@ionic/vue'

const app = createApp(App)
  .use(IonicVue)
  .use(router)
  .use(store)

app.component('BaseLayout', BaseLayout)
app.component('IonImg', IonImg)
app.component('IonThumbnail', IonThumbnail)
app.component('IonRow', IonRow)
app.component('IonCol', IonCol)
app.component('IonCard', IonCard)
app.component('IonCardHeader', IonCardHeader)
app.component('IonCardTitle', IonCardTitle)
app.component('IonChip', IonChip)
app.component('IonText', IonText)
app.component('IonItem', IonItem)
  
router.isReady().then(() => {
  app.mount('#app')
})